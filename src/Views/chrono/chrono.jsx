import { useEffect, useState } from 'react';
import './chrono.css';
import door from '../../assets/images/door.png';
import zodiaque from '../../assets/images/zodiaque.png';
import mqtt from 'mqtt';

export default function Chrono() {
  const [connectStatus, setConnectStatus] = useState('connecting');
  const [remainingTime, setRemainingTime] = useState(120);
  const [isEnigma1Active, setIsEnigma1Active] = useState(true);
  const [isEnigma2Active, setIsEnigma2Active] = useState(false);
  const [isWin, setIsWin] = useState(false);

  const client = mqtt.connect('ws://broker.emqx.io:8083/mqtt');

  const roomID = 1;
  const pressureSensorTopic = `/escape-game/rooms/${roomID}/pressure-sensor`;
  const magneticStrikerTopic = `/escape-game/rooms/${roomID}/magnetic-striker`;
  const photoresistorTopic = `/escape-game/rooms/${roomID}/photoresistor-sensor`;

  useEffect(() => {
    const intervalId = setInterval(() => {
      setRemainingTime((prevRemainingTime) => {
        if (prevRemainingTime > 0) {
          return prevRemainingTime - 1;
        } else {
          clearInterval(intervalId);
          return prevRemainingTime;
        }
      });
    }, 1000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    client.subscribe(pressureSensorTopic, (err) => {
      if (err) console.log('error subscribing to pressure-sensor topic');
    });
    client.subscribe(photoresistorTopic, (err) => {
      if (err) console.log('error subscribing to pressure-sensor topic');
    });

    client.on('connect', () => setConnectStatus('connected'));
    client.on('reconnect', () => setConnectStatus('reconnecting'));
    client.on('error', (err) => {
      console.error('Connection error: ', err);
      setConnectStatus('error');
      client.end();
    });
    client.on('message', (topic, message) => {
      if (topic === photoresistorTopic) {
        setIsEnigma1Active(false);
        setIsEnigma2Active(true);
      }
      if (topic === pressureSensorTopic) {
        setIsEnigma2Active(false);
        setIsWin(true);
      }
    });
    console.log(connectStatus);
  }, [
    client,
    connectStatus,
    magneticStrikerTopic,
    photoresistorTopic,
    pressureSensorTopic
  ]);

  const minutes = Math.floor(remainingTime / 60);
  const seconds = remainingTime % 60;

  return (
    <div className="container">
      <div>
        Temps restant : {minutes} min {seconds} sec
      </div>
      {remainingTime === 0 ? (
        <div className="loose"> Vous avez perdu !</div>
      ) : (
        <>
          {isEnigma1Active && !isWin && (
            <p>Je me trouve proche d une source d energie, trouve moi</p>
          )}
          {isEnigma2Active && !isWin && (
            <>
              <img src={zodiaque} alt="zodiaque" className="zodiaque" />
              <div>
                Déterminé et courageux, j'avance sans bornes. Qui suis-je ?
              </div>
            </>
          )}
          {isWin && (
            <div className="container-win">
              <div className="description-win">
                <div>
                  Bravo vous avez réussi à ouvrir la porte ! Fuyez sans vous
                  retourner, amélie est juste derrière vous...
                </div>
              </div>
              <img src={door} alt="door" className="door" />
            </div>
          )}
        </>
      )}
    </div>
  );
}
