import './enigme.css';
import amelie from '../../assets/images/amelie.png';
import couteau from '../../assets/images/couteau.png';
import { ChevronRight } from 'react-feather';
import { Link } from 'react-router-dom';

export default function Enigme() {
  return (
    <div className="container">
      <div className="pictures">
        <img src={amelie} alt="amelie" className="amelie" />
        <img src={couteau} alt="couteau" className="couteau" />
      </div>
      <div className="description">
        Votre amie Amélie aime invoquer satan, et elle l'a provoqué hier soir.
        Aujourd'hui pendant votre cours d'iot, elle s'est mise à convulser et à
        crier, elle a sortie un couteau et planté un camarade de classe ! Il
        faut vite vous enfuir, mais la porte est fermée, à vous de résoudre
        l'énigme en 2 minutes si vous ne voulez pas mourir.
      </div>
      <Link to={'/chrono'} className="buttonStart">
        <ChevronRight size={28} color={'black'} />
      </Link>
    </div>
  );
}
