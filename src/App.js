import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import mqtt from 'mqtt';

function App() {
  const [connectStatus, setConnectStatus] = useState('Connecting...');
  const client = mqtt.connect('ws://broker.emqx.io:8083/mqtt');

  useEffect(() => {
    client.subscribe('/pressure-sensor', (err) => {
      if (err) console.log('error subscribing to pressure-sensor topic');
    });

    client.on('connect', () => setConnectStatus('Connected'));
    client.on('reconnect', () => setConnectStatus('Reconnecting...'));
    client.on('error', (err) => {
      console.error('Connection error: ', err);
      client.end();
    });
    client.on('message', (topic, message) => {
      const data = JSON.parse(message.toString());
      console.log({ topic, data });
    });
  }, [client]);

  console.log(connectStatus);

  return (
    <div className="App">
      <p>Bienvenue dans notre escape game</p>
      <Link to={'/enigme'}>
        <button className="button">Commencer</button>
      </Link>
    </div>
  );
}

export default App;
